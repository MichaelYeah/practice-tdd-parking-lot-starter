package com.parkinglot;

import com.parkinglot.Exceptions.NoAvailablePositionException;
import com.parkinglot.Exceptions.UnrecognizedParkingTicketException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SmartParkingBoyTest {
    @Test
    void should_the_car_be_parked_to_the_parking_lot_containing_more_empty_spaces_when_park_given_smart_parking_boy_managing_two_parking_lot_and_car() {
        //given
        ParkingLot parkingLotContainingMoreEmptySpaces = new ParkingLot(1);
        ParkingLot parkingLotContainingLessEmptySpaces = new ParkingLot(2);
        SmartParkingBoy standardParkingBoy = new SmartParkingBoy(parkingLotContainingMoreEmptySpaces, parkingLotContainingLessEmptySpaces);
        int aNumber = ParkingLot.Capacity - 3;
        int i = 0;
        for (; i < aNumber; i++) {
            parkingLotContainingMoreEmptySpaces.park(new Car(i));
        }
        for (; i < aNumber + aNumber + 1; i++) {
            parkingLotContainingLessEmptySpaces.park(new Car(i));
        }

        //when
        ParkingTicket ticket = standardParkingBoy.park(new Car(i));

        //then
        assertNotNull(ticket);
        assertEquals(parkingLotContainingMoreEmptySpaces.getParkingLotId(), ticket.getParkingLotId());
    }

    @Test
    void should_return_right_parked_car_with_each_ticket_when_fetch_given_smart_parking_boy_managing_two_parking_lot_and_two_car_parking_at_different_parking_lot() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(2);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLot1, parkingLot2);
        Car car1 = new Car(1);
        ParkingTicket ticket1 = smartParkingBoy.park(car1);
        Car car2 = new Car(2);
        ParkingTicket ticket2 = smartParkingBoy.park(car2);

        //when
        Car carFetched1 = smartParkingBoy.fetch(ticket1);
        Car carFetched2 = smartParkingBoy.fetch(ticket2);

        //then
        assertEquals(car1, carFetched1);
        assertEquals(car2, carFetched2);
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_given_parking_lot_and_smart_parking_boy_managing_two_parking_lots_and_unrecognized_ticket() {
        //given
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(new ParkingLot(1), new ParkingLot(2));
        Car car = new Car(1);
        ParkingTicket usedTicket = smartParkingBoy.park(car);
        smartParkingBoy.fetch(usedTicket);
        ParkingTicket wrongTicket = new ParkingTicket(car.getCarNo(), 1);

        //when

        //then
        var exceptionThrowAfterFetchedCarWithUsedTicket = assertThrows(UnrecognizedParkingTicketException.class, () -> smartParkingBoy.fetch(usedTicket));
        assertEquals("Unrecognized parking ticket.", exceptionThrowAfterFetchedCarWithUsedTicket.getMessage());
        var exceptionThrowAfterFetchedCarWithWrongTicket = assertThrows(UnrecognizedParkingTicketException.class, () -> smartParkingBoy.fetch(wrongTicket));
        assertEquals("Unrecognized parking ticket.", exceptionThrowAfterFetchedCarWithWrongTicket.getMessage());
    }

    @Test
    void should_throw_exception_with_error_message_when_park_given_full_parking_lot_and_smart_parking_boy_managing_two_full_parking_lots_and_car() {
        //given
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(new ParkingLot(1), new ParkingLot(2));
        for (int i = 0; i < ParkingLot.Capacity * 2; i++) {
            smartParkingBoy.park(new Car(i + 1));
        }

        //when

        //then
        var exception = assertThrows(NoAvailablePositionException.class, () -> smartParkingBoy.park(new Car(ParkingLot.Capacity * 2)));
        assertEquals("No available position.", exception.getMessage());
    }
}