package com.parkinglot;

import com.parkinglot.Exceptions.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ParkingServiceManagerTest {
    @Test
    void should_return_ticket_when_specify_a_managed_parking_boy_to_park_given_parking_service_manager_and_super_smart_parking_boy_and_car() {
        //given
        ParkingServiceManager parkingServiceManager = new ParkingServiceManager();
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(new ParkingLot(1));
        parkingServiceManager.addParkingBoysToManagedList(superSmartParkingBoy);

        //when
        ParkingTicket ticket = parkingServiceManager.specifyAParkingBoyToPark(superSmartParkingBoy, new Car(1));

        //then
        assertNotNull(ticket);
    }

    @Test
    void should_throw_exception_with_error_message_when_specify_a_unmanaged_parking_boy_to_park_given_parking_service_manager_and_parking_service_manager_and_car() {
        //given
        ParkingServiceManager parkingServiceManager = new ParkingServiceManager();
        parkingServiceManager.addParkingBoysToManagedList(new StandardParkingBoy(new ParkingLot(1)));
        StandardParkingBoy unmanagedStandardParkingBoy = new StandardParkingBoy(new ParkingLot(1));

        //when

        //then
        var exception = assertThrows(NotManageSuchParkingBoyException.class, () -> parkingServiceManager.specifyAParkingBoyToPark(unmanagedStandardParkingBoy, new Car(1)));
        assertEquals("No this parking boy in the manage list.", exception.getMessage());
    }

    void should_the_car_be_parked_to_the_first_parking_lot_when_park_given_parking_service_manager_managing_two_available_parking_lot_and_car() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(2);
        ParkingServiceManager parkingServiceManager = new ParkingServiceManager(parkingLot1, parkingLot2);
        Car car = new Car(1);

        //when
        ParkingTicket ticket = parkingServiceManager.park(car);

        //then
        assertNotNull(ticket);
        assertEquals(parkingLot1.getParkingLotId(), ticket.getParkingLotId());
    }
    @Test
    void should_the_car_be_parked_to_the_second_parking_lot_when_park_given_parking_service_manager_managing_full_parking_lot_and_available_parking_lot__and_car() {
        //given
        ParkingLot fullParkingLot = new ParkingLot(1);
        ParkingLot availableParkingLot = new ParkingLot(2);
        ParkingServiceManager parkingServiceManager = new ParkingServiceManager(fullParkingLot, availableParkingLot);for (int i = 0; i < ParkingLot.Capacity; i++) {
            parkingServiceManager.park(new Car(i + 1));
        }

        //when
        ParkingTicket ticket = parkingServiceManager.park(new Car(ParkingLot.Capacity));

        //then
        assertNotNull(ticket);
        assertEquals(availableParkingLot.getParkingLotId(), ticket.getParkingLotId());
    }

    @Test
    void should_return_right_parked_car_with_each_ticket_when_fetch_given_parking_service_manager_managing_two_parking_lot_and_two_car_parking_at_different_parking_lot() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(2);
        ParkingServiceManager parkingServiceManager = new ParkingServiceManager(parkingLot1, parkingLot2);
        for (int i = 0; i < ParkingLot.Capacity - 1; i++) {
            parkingServiceManager.park(new Car(i + 1));
        }
        Car car1 = new Car(ParkingLot.Capacity - 1);
        ParkingTicket ticket1 = parkingServiceManager.park(car1);
        Car car2 = new Car(ParkingLot.Capacity);
        ParkingTicket ticket2 = parkingServiceManager.park(car2);

        //when
        Car carFetched1 = parkingServiceManager.fetch(ticket1);
        Car carFetched2 = parkingServiceManager.fetch(ticket2);

        //then
        assertEquals(car1, carFetched1);
        assertEquals(car2, carFetched2);
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_given_parking_lot_and_parking_service_manager_managing_two_parking_lots_and_unrecognized_ticket() {
        //given
        ParkingServiceManager parkingServiceManager = new ParkingServiceManager(new ParkingLot(1), new ParkingLot(2));
        Car car = new Car(1);
        ParkingTicket usedTicket = parkingServiceManager.park(car);
        parkingServiceManager.fetch(usedTicket);
        ParkingTicket wrongTicket = new ParkingTicket(car.getCarNo(), 1);

        //when

        //then
        var exceptionThrowAfterFetchedCarWithUsedTicket = assertThrows(UnrecognizedParkingTicketException.class, () -> parkingServiceManager.fetch(usedTicket));
        assertEquals("Unrecognized parking ticket.", exceptionThrowAfterFetchedCarWithUsedTicket.getMessage());
        var exceptionThrowAfterFetchedCarWithWrongTicket = assertThrows(UnrecognizedParkingTicketException.class, () -> parkingServiceManager.fetch(wrongTicket));
        assertEquals("Unrecognized parking ticket.", exceptionThrowAfterFetchedCarWithWrongTicket.getMessage());
    }

    @Test
    void should_throw_exception_with_error_message_when_park_given_full_parking_lot_and_parking_service_manager_managing_two_full_parking_lots_and_car() {
        //given
        ParkingServiceManager parkingServiceManager = new ParkingServiceManager(new ParkingLot(1), new ParkingLot(2));
        for (int i = 0; i < ParkingLot.Capacity * 2; i++) {
            parkingServiceManager.park(new Car(i + 1));
        }

        //when

        //then
        var exception = assertThrows(NoAvailablePositionException.class, () -> parkingServiceManager.park(new Car(ParkingLot.Capacity * 2)));
        assertEquals("No available position.", exception.getMessage());
    }

    @Test
    void should_return_right_parked_car_with_each_ticket_when_fetch_given_parking_service_manager_managing_two_parking_boys_managing_two_parking_lots_and_four_car_parking_at_different_parking_lot() {
        //given
        ParkingServiceManager parkingServiceManager = new ParkingServiceManager();

        ParkingLot parkingLot1 = new ParkingLot(1, 10);
        ParkingLot parkingLot2 = new ParkingLot(2, 8);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLot1, parkingLot2);
        Car car1 = new Car(1);
        ParkingTicket ticket1 = superSmartParkingBoy.park(car1);
        Car car2 = new Car(2);
        ParkingTicket ticket2 = superSmartParkingBoy.park(car2);

        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLot1, parkingLot2);
        Car car3 = new Car(3);
        ParkingTicket ticket3 = smartParkingBoy.park(car3);
        Car car4 = new Car(4);
        ParkingTicket ticket4 = smartParkingBoy.park(car4);

        parkingServiceManager.addParkingBoysToManagedList(superSmartParkingBoy, smartParkingBoy);

        //when
        Car car1FetchedBySuperSmartParkingBoy = parkingServiceManager.specifyAParkingBoyToFetch(superSmartParkingBoy, ticket1);
        Car car2FetchedBySuperSmartParkingBoy = parkingServiceManager.specifyAParkingBoyToFetch(superSmartParkingBoy, ticket2);
        Car car3FetchedBySmartParkingBoy = parkingServiceManager.specifyAParkingBoyToFetch(smartParkingBoy, ticket3);
        Car car4FetchedBySmartParkingBoy = parkingServiceManager.specifyAParkingBoyToFetch(smartParkingBoy, ticket4);

        //then
        assertEquals(car1, car1FetchedBySuperSmartParkingBoy);
        assertEquals(car2, car2FetchedBySuperSmartParkingBoy);
        assertEquals(car3, car3FetchedBySmartParkingBoy);
        assertEquals(car4, car4FetchedBySmartParkingBoy);
    }

    @Test
    void should_throw_exception_with_error_when_specify_a_parking_boy_to_fetch_given_parking_service_manager_and_unrecognized_ticket() {
        //given
        ParkingServiceManager parkingServiceManager = new ParkingServiceManager();
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(new ParkingLot(1), new ParkingLot(2));
        Car car = new Car(1);
        ParkingTicket usedTicket = smartParkingBoy.park(car);
        smartParkingBoy.fetch(usedTicket);
        ParkingTicket wrongTicket = new ParkingTicket(car.getCarNo(), 1);
        parkingServiceManager.addParkingBoysToManagedList(smartParkingBoy);

        //when

        //then
        var exceptionThrowAfterFetchedCarWithUsedTicket = assertThrows(ManagedParkingBoyFailToFetchException.class, () -> parkingServiceManager.specifyAParkingBoyToFetch(smartParkingBoy, usedTicket));
        assertEquals("The managed parking boy failed to fetch a car.", exceptionThrowAfterFetchedCarWithUsedTicket.getMessage());
        var exceptionThrowAfterFetchedCarWithWrongTicket = assertThrows(ManagedParkingBoyFailToFetchException.class, () -> parkingServiceManager.specifyAParkingBoyToFetch(smartParkingBoy, wrongTicket));
        assertEquals("The managed parking boy failed to fetch a car.", exceptionThrowAfterFetchedCarWithWrongTicket.getMessage());
    }

    @Test
    void should_throw_exception_with_error_message_when_specify_a_parking_boy_to_park_given_parking_service_manager_managing_standard_parking_boy_managing_full_parking_lot_and_car() {
        //given
        ParkingServiceManager parkingServiceManager = new ParkingServiceManager();
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(new ParkingLot(1));
        for (int i = 0; i < ParkingLot.Capacity; i++) {
            standardParkingBoy.park(new Car(i));
        }
        parkingServiceManager.addParkingBoysToManagedList(standardParkingBoy);

        //when

        //then
        var exception = assertThrows(ManagedParkingBoyFailToParkException.class, () -> parkingServiceManager.specifyAParkingBoyToPark(standardParkingBoy, new Car(ParkingLot.Capacity)));
        assertEquals("The managed parking boy failed to park a car.", exception.getMessage());
    }

}