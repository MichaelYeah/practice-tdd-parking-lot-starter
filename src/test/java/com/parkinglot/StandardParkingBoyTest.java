package com.parkinglot;

import com.parkinglot.Exceptions.NoAvailablePositionException;
import com.parkinglot.Exceptions.UnrecognizedParkingTicketException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StandardParkingBoyTest {
    @Test
    void should_the_car_be_parked_to_the_first_parking_lot_when_park_given_standard_parking_boy_managing_two_available_parking_lot_and_car() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(2);
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLot1, parkingLot2);
        Car car = new Car(1);

        //when
        ParkingTicket ticket = standardParkingBoy.park(car);

        //then
        assertNotNull(ticket);
        assertEquals(parkingLot1.getParkingLotId(), ticket.getParkingLotId());
    }
    @Test
    void should_the_car_be_parked_to_the_second_parking_lot_when_park_given_standard_parking_boy_managing_full_parking_lot_and_available_parking_lot__and_car() {
        //given
        ParkingLot fullParkingLot = new ParkingLot(1);
        ParkingLot availableParkingLot = new ParkingLot(2);
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(fullParkingLot, availableParkingLot);for (int i = 0; i < ParkingLot.Capacity; i++) {
            standardParkingBoy.park(new Car(i + 1));
        }

        //when
        ParkingTicket ticket = standardParkingBoy.park(new Car(ParkingLot.Capacity));

        //then
        assertNotNull(ticket);
        assertEquals(availableParkingLot.getParkingLotId(), ticket.getParkingLotId());
    }
    
    @Test
    void should_return_right_parked_car_with_each_ticket_when_fetch_given_standard_parking_boy_managing_two_parking_lot_and_two_car_parking_at_different_parking_lot() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(2);
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLot1, parkingLot2);
        for (int i = 0; i < ParkingLot.Capacity - 1; i++) {
            standardParkingBoy.park(new Car(i + 1));
        }
        Car car1 = new Car(ParkingLot.Capacity - 1);
        ParkingTicket ticket1 = standardParkingBoy.park(car1);
        Car car2 = new Car(ParkingLot.Capacity);
        ParkingTicket ticket2 = standardParkingBoy.park(car2);

        //when
        Car carFetched1 = standardParkingBoy.fetch(ticket1);
        Car carFetched2 = standardParkingBoy.fetch(ticket2);

        //then
        assertEquals(car1, carFetched1);
        assertEquals(car2, carFetched2);
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_given_parking_lot_and_standard_parking_boy_managing_two_parking_lots_and_unrecognized_ticket() {
        //given
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(new ParkingLot(1), new ParkingLot(2));
        Car car = new Car(1);
        ParkingTicket usedTicket = standardParkingBoy.park(car);
        standardParkingBoy.fetch(usedTicket);
        ParkingTicket wrongTicket = new ParkingTicket(car.getCarNo(), 1);

        //when

        //then
        var exceptionThrowAfterFetchedCarWithUsedTicket = assertThrows(UnrecognizedParkingTicketException.class, () -> standardParkingBoy.fetch(usedTicket));
        assertEquals("Unrecognized parking ticket.", exceptionThrowAfterFetchedCarWithUsedTicket.getMessage());
        var exceptionThrowAfterFetchedCarWithWrongTicket = assertThrows(UnrecognizedParkingTicketException.class, () -> standardParkingBoy.fetch(wrongTicket));
        assertEquals("Unrecognized parking ticket.", exceptionThrowAfterFetchedCarWithWrongTicket.getMessage());
    }

    @Test
    void should_throw_exception_with_error_message_when_park_given_full_parking_lot_and_standard_parking_boy_managing_two_full_parking_lots_and_car() {
        //given
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(new ParkingLot(1), new ParkingLot(2));
        for (int i = 0; i < ParkingLot.Capacity * 2; i++) {
            standardParkingBoy.park(new Car(i + 1));
        }

        //when

        //then
        var exception = assertThrows(NoAvailablePositionException.class, () -> standardParkingBoy.park(new Car(ParkingLot.Capacity * 2)));
        assertEquals("No available position.", exception.getMessage());
    }

}