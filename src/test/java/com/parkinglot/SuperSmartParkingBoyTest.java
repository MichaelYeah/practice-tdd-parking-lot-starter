package com.parkinglot;

import com.parkinglot.Exceptions.NoAvailablePositionException;
import com.parkinglot.Exceptions.UnrecognizedParkingTicketException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SuperSmartParkingBoyTest {
    @Test
    void should_the_car_be_parked_to_the_parking_lot_having_lower_rate_of_empty_spaces_when_park_given_super_smart_parking_boy_managing_two_available_parking_lot_and_car() {
        //given
        ParkingLot parkingLotHavingLowerRateOfEmptySpaces = new ParkingLot(1, 10);
        parkingLotHavingLowerRateOfEmptySpaces.park(new Car(1));
        ParkingLot parkingLotHavingHigherRateOfEmptySpaces = new ParkingLot(2, 8);
        parkingLotHavingHigherRateOfEmptySpaces.park(new Car(2));
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLotHavingLowerRateOfEmptySpaces, parkingLotHavingHigherRateOfEmptySpaces);

        //when
        ParkingTicket ticket = superSmartParkingBoy.park(new Car(3));

        //then
        assertNotNull(ticket);
        assertEquals(parkingLotHavingLowerRateOfEmptySpaces.getParkingLotId(), ticket.getParkingLotId());
    }

    @Test
    void should_return_right_parked_car_with_each_ticket_when_fetch_given_super_smart_parking_boy_managing_two_parking_lot_and_two_car_parking_at_different_parking_lot() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1, 10);
        ParkingLot parkingLot2 = new ParkingLot(2, 8);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLot1, parkingLot2);
        Car car1 = new Car(1);
        ParkingTicket ticket1 = superSmartParkingBoy.park(car1);
        Car car2 = new Car(2);
        ParkingTicket ticket2 = superSmartParkingBoy.park(car2);

        //when
        Car carFetched1 = superSmartParkingBoy.fetch(ticket1);
        Car carFetched2 = superSmartParkingBoy.fetch(ticket2);

        //then
        assertEquals(car1, carFetched1);
        assertEquals(car2, carFetched2);
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_given_parking_lot_and_super_smart_parking_boy_managing_two_parking_lots_and_unrecognized_ticket() {
        //given
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(new ParkingLot(1), new ParkingLot(2));
        Car car = new Car(1);
        ParkingTicket usedTicket = smartParkingBoy.park(car);
        smartParkingBoy.fetch(usedTicket);
        ParkingTicket wrongTicket = new ParkingTicket(car.getCarNo(), 1);

        //when

        //then
        var exceptionThrowAfterFetchedCarWithUsedTicket = assertThrows(UnrecognizedParkingTicketException.class, () -> smartParkingBoy.fetch(usedTicket));
        assertEquals("Unrecognized parking ticket.", exceptionThrowAfterFetchedCarWithUsedTicket.getMessage());
        var exceptionThrowAfterFetchedCarWithWrongTicket = assertThrows(UnrecognizedParkingTicketException.class, () -> smartParkingBoy.fetch(wrongTicket));
        assertEquals("Unrecognized parking ticket.", exceptionThrowAfterFetchedCarWithWrongTicket.getMessage());
    }

    @Test
    void should_throw_exception_with_error_message_when_park_given_full_parking_lot_and_super_smart_parking_boy_managing_two_full_parking_lots_and_car() {
        //given
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(new ParkingLot(1), new ParkingLot(2));
        for (int i = 0; i < ParkingLot.Capacity * 2; i++) {
            smartParkingBoy.park(new Car(i + 1));
        }

        //when

        //then
        var exception = assertThrows(NoAvailablePositionException.class, () -> smartParkingBoy.park(new Car(ParkingLot.Capacity * 2)));
        assertEquals("No available position.", exception.getMessage());
    }
}