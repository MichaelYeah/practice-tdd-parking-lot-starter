package com.parkinglot;

import com.parkinglot.Exceptions.NoAvailablePositionException;
import com.parkinglot.Exceptions.UnrecognizedParkingTicketException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ParkingLotTest {
    @Test
    void should_return_ticket_when_park_given_parking_lot_and_car() {
        //given
        ParkingLot parkingLot = new ParkingLot(1);
        Car car = new Car(1);

        //when
        ParkingTicket ticket = parkingLot.park(car);

        //then
        assertNotNull(ticket);
    }
    @Test
    void should_return_car_when_fetch_given_parking_lot_and_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(1);
        Car car = new Car(1);
        ParkingTicket parkingTicket = parkingLot.park(car);

        //when
        Car fetchCar = parkingLot.fetch(parkingTicket);

        //then
        assertEquals(car, fetchCar);
    }
    
    @Test
    void should_return_two_right_car_when_park_twice_first_and_fetch_twice_later_given_parking_lot_and_two_cars() {
        //given
        ParkingLot parkingLot = new ParkingLot(1);
        Car car1 = new Car(1);
        Car car2 = new Car(2);

        //when
        ParkingTicket parkingTicket1 = parkingLot.park(car1);
        ParkingTicket parkingTicket2 = parkingLot.park(car2);
        Car carFetched1 = parkingLot.fetch(parkingTicket1);
        Car carFetched2 = parkingLot.fetch(parkingTicket2);

        //then
        assertEquals(car1, carFetched1);
        assertEquals(car2, carFetched2);
    }
    
    @Test
    void should_throw_exception_with_error_message_when_fetch_given_parking_lot_and_wrong_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(1);
        Car car1 = new Car(1);
        parkingLot.park(car1);
        ParkingTicket ticketOfCar1 = new ParkingTicket(car1.getCarNo(), parkingLot.getParkingLotId());
        ParkingTicket unrecognizedTicket = new ParkingTicket(2, 1);
        parkingLot.fetch(ticketOfCar1);

        //when

        //then
        var exceptionThrowsAfterFetchCarWithUnRecognizedTicket = assertThrows(UnrecognizedParkingTicketException.class, () -> parkingLot.fetch(unrecognizedTicket));
        assertEquals("Unrecognized parking ticket.", exceptionThrowsAfterFetchCarWithUnRecognizedTicket.getMessage());
        var exceptionThrowsAfterFetchCarWithUsedTicket = assertThrows(UnrecognizedParkingTicketException.class, () -> parkingLot.fetch(ticketOfCar1));
        assertEquals("Unrecognized parking ticket.", exceptionThrowsAfterFetchCarWithUsedTicket.getMessage());
    }
    
    @Test
    void should_return_nothing_when_fetch_given_parking_lot_used_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(1);
        Car car1 = new Car(1);
        parkingLot.park(car1);
        ParkingTicket parkingTicketOfCar1 = new ParkingTicket(car1.getCarNo(), parkingLot.getParkingLotId());
        Car carFetchedByParkingTicketOfCar1 = parkingLot.fetch(parkingTicketOfCar1);
     
        //when

        //then
        var exception = assertThrows(UnrecognizedParkingTicketException.class, () -> parkingLot.fetch(parkingTicketOfCar1));
        assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }
    
    @Test
    void should_throw_exception_with_error_message_when_park_given_full_parking_lot() {
        //given
        ParkingLot parkingLot = new ParkingLot(1);
        int i;
        for (i = 0; i < ParkingLot.Capacity; i++) {
            parkingLot.park(new Car(i + 1));
        }

        //when

        //then
        int finalI = i;
        var exception = assertThrows(NoAvailablePositionException.class, () -> parkingLot.park(new Car(finalI)));
        assertEquals("No available position.", exception.getMessage());
    }

}
