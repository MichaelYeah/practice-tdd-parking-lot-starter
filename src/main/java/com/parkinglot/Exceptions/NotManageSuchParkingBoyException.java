package com.parkinglot.Exceptions;

public class NotManageSuchParkingBoyException extends RuntimeException {
    public NotManageSuchParkingBoyException() {
        super("No this parking boy in the manage list.");
    }
}
