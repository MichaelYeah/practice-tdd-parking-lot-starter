package com.parkinglot.Exceptions;

public class ManagedParkingBoyFailToFetchException extends RuntimeException {
    public ManagedParkingBoyFailToFetchException() {
        super("The managed parking boy failed to fetch a car.");
    }
}
