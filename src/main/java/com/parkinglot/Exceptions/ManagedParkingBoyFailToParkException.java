package com.parkinglot.Exceptions;

public class ManagedParkingBoyFailToParkException extends RuntimeException {
    public ManagedParkingBoyFailToParkException() {
        super("The managed parking boy failed to park a car.");
    }
}
