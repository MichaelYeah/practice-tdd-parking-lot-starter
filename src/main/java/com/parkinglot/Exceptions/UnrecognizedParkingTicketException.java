package com.parkinglot.Exceptions;

public class UnrecognizedParkingTicketException extends RuntimeException {
    public UnrecognizedParkingTicketException() {
        super("Unrecognized parking ticket.");
    }
}
