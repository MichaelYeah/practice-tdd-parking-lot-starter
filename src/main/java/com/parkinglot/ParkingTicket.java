package com.parkinglot;

public class ParkingTicket {
    private int carNo;

    private int parkingLotId;

    public ParkingTicket(int carNo, int parkingLotId) {
        this.carNo = carNo;
        this.parkingLotId = parkingLotId;
    }

    public int getCarNo() {
        return carNo;
    }

    public int getParkingLotId() {
        return parkingLotId;
    }
}
