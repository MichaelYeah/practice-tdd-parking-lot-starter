package com.parkinglot;

import com.parkinglot.strategy.SequentialGetFreeParkingLotStrategy;

public class StandardParkingBoy extends ParkingBoy {
    public StandardParkingBoy(ParkingLot... parkingLots) {
        super(parkingLots);
        this.getFreeParkingLotStrategy = new SequentialGetFreeParkingLotStrategy();
    }
}
