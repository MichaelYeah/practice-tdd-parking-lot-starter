package com.parkinglot;

import com.parkinglot.Exceptions.NoAvailablePositionException;
import com.parkinglot.Exceptions.UnrecognizedParkingTicketException;

import java.util.ArrayList;
import java.util.List;

public class ParkingLot {
    public final static int Capacity = 10;
    public int parkingLotId = -1;
    private int actualCapacity = Capacity;
    private List<Car> cars = new ArrayList<>();

    public ParkingLot(int parkingLotId) {
        this.parkingLotId = parkingLotId;
    }

    public ParkingLot(int parkingLotId, int capacity) {
        this(parkingLotId);
        actualCapacity = capacity;
    }

    public ParkingTicket park(Car car) {
        if (!isFull()) {
            this.cars.add(car);
            return new ParkingTicket(car.getCarNo(), parkingLotId);
        }
        throw new NoAvailablePositionException();
    }

    public boolean isFull() {
        if (cars.size() >= Capacity) return true;
        return false;
    }

    public Car fetch(ParkingTicket parkingTicket) {
        Car car = findCarWithParkingTicket(parkingTicket);
        if (car != null) {
            cars.remove(car);
            return car;
        }
        throw new UnrecognizedParkingTicketException();
    }

    public Car findCarWithParkingTicket(ParkingTicket parkingTicket) {
        Car carFetched = cars.stream().filter(car -> car.getCarNo() == parkingTicket.getCarNo()).findFirst().orElse(null);
        if (carFetched != null) {
            return carFetched;
        }
        return null;
    }

    public int getParkingLotId() {
        return parkingLotId;
    }

    public int getNumberOfEmptyParkingSpace() {
        return this.actualCapacity - cars.size();
    }

    public int getActualCapacity() {
        return actualCapacity;
    }

    public double getParkingSpacesEmptyRate() {
        return (double) getNumberOfEmptyParkingSpace() / actualCapacity;
    }
}
