package com.parkinglot;

import java.util.Objects;

public class Car {
    private int carNo;

    public Car(int carNo) {
        this.carNo = carNo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return carNo == car.carNo;
    }

    @Override
    public int hashCode() {
        return Objects.hash(carNo);
    }

    public int getCarNo() {
        return carNo;
    }
}
