package com.parkinglot;

import com.parkinglot.Exceptions.NoAvailablePositionException;
import com.parkinglot.Exceptions.UnrecognizedParkingTicketException;
import com.parkinglot.strategy.GetFreeParkingLotStrategy;
import com.parkinglot.strategy.SequentialGetFreeParkingLotStrategy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public abstract class ParkingBoy {
    protected final List<ParkingLot> managedLots = new ArrayList<>();
    protected GetFreeParkingLotStrategy getFreeParkingLotStrategy = new SequentialGetFreeParkingLotStrategy();

    protected ParkingBoy(ParkingLot... parkingLots) {
        Collections.addAll(managedLots, parkingLots);
    }

    public ParkingTicket park(Car car) {
        ParkingLot freeParkingLot = getFreeParkingLot();
        if (freeParkingLot != null) return freeParkingLot.park(car);
        throw new NoAvailablePositionException();
    }

    public Car fetch(ParkingTicket ticket) {
        ParkingLot parkingLotByTicket = getParkingLotByTicket(ticket);
        if (parkingLotByTicket != null) return parkingLotByTicket.fetch(ticket);
        throw new UnrecognizedParkingTicketException();
    }

    public ParkingLot getParkingLotByTicket(ParkingTicket ticket) {
        return managedLots.stream().filter(parkingLot -> parkingLot.findCarWithParkingTicket(ticket) != null).findFirst().orElse(null);
    }

    protected ParkingLot getFreeParkingLot() {
        return getFreeParkingLotStrategy.getFreeParkingLot(managedLots);
    }

}
