package com.parkinglot;

import com.parkinglot.Exceptions.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ParkingServiceManager extends StandardParkingBoy {
    List<ParkingBoy> managedParkingBoys = new ArrayList<>();

    public void addParkingBoysToManagedList(ParkingBoy... parkingBoys) {
        Collections.addAll(managedParkingBoys, parkingBoys);
    }

    public ParkingTicket specifyAParkingBoyToPark(ParkingBoy parkingBoy, Car car) {
        try {
            return isParkingBoyManged(parkingBoy) ? parkingBoy.park(car) : null;
        } catch (NoAvailablePositionException e) {
            throw new ManagedParkingBoyFailToParkException();
        }
    }

    public Car specifyAParkingBoyToFetch(ParkingBoy parkingBoy, ParkingTicket ticket) {
        try {
            return isParkingBoyManged(parkingBoy) ? parkingBoy.fetch(ticket) : null;
        } catch (UnrecognizedParkingTicketException e) {
            throw new ManagedParkingBoyFailToFetchException();
        }
    }

    public boolean isParkingBoyManged(ParkingBoy parkingBoy) {
        if (managedParkingBoys.contains(parkingBoy)) return true;
        throw new NotManageSuchParkingBoyException();
    }

    public ParkingServiceManager(ParkingLot... parkingLots) {
        super(parkingLots);
    }
}
