package com.parkinglot;

import com.parkinglot.strategy.MoreEmptySpacesFirstGetFreeParkingLotStrategy;

public class SmartParkingBoy extends ParkingBoy {
    public SmartParkingBoy(ParkingLot... parkingLots) {
        super(parkingLots);
        this.getFreeParkingLotStrategy = new MoreEmptySpacesFirstGetFreeParkingLotStrategy();
    }
}
