package com.parkinglot.strategy;

import com.parkinglot.ParkingLot;

import java.util.List;

public interface GetFreeParkingLotStrategy {
    ParkingLot getFreeParkingLot(List<ParkingLot> managedLots);
}
