package com.parkinglot.strategy;

import com.parkinglot.ParkingLot;

import java.util.List;

public class SequentialGetFreeParkingLotStrategy implements GetFreeParkingLotStrategy {
    @Override
    public ParkingLot getFreeParkingLot(List<ParkingLot> managedLots) {
        return managedLots.stream().filter(parkingLot -> !parkingLot.isFull()).findFirst().orElse(null);
    }
}
