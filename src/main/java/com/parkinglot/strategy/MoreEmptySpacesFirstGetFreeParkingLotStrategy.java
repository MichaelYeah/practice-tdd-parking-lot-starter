package com.parkinglot.strategy;

import com.parkinglot.ParkingLot;

import java.util.Comparator;
import java.util.List;

public class MoreEmptySpacesFirstGetFreeParkingLotStrategy implements GetFreeParkingLotStrategy {
    @Override
    public ParkingLot getFreeParkingLot(List<ParkingLot> managedLots) {
        return managedLots.stream().max(Comparator.comparingInt(ParkingLot::getNumberOfEmptyParkingSpace)).orElse(null);
    }
}
