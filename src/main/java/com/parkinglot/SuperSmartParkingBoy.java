package com.parkinglot;

import com.parkinglot.strategy.LessEmptyRateFirstGetFreeParkingLotStrategy;

public class SuperSmartParkingBoy extends ParkingBoy {
    public SuperSmartParkingBoy(ParkingLot... parkingLots) {
        super(parkingLots);
        this.getFreeParkingLotStrategy = new LessEmptyRateFirstGetFreeParkingLotStrategy();
    }
}
